package com.example.homeworkapp1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import java.util.*

class MainActivity : AppCompatActivity() {

    private var numLeft = 0
    private var numRight = 0
    private var counter = 0
    private var score = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        pickNumbers()
    }

    fun leftButtonClick(view: View) {
        if(numLeft>numRight)
            counter++
        else
            counter--
        score++
        pickNumbers()
    }
    fun rightButtonClick(view: View) {
        if(numLeft<numRight)
            counter++
        else
            counter--
        score++
        pickNumbers()
    }

    fun startNewGame(view: View){
        //delete text about last score
        findViewById<TextView>(R.id.textViewInfo).text = " "
        score = 0
        counter = 0
        pickNumbers()
        //hide button "Start new game"
        findViewById<Button>(R.id.buttonStartNewGame).visibility = View.GONE
    }

    fun pickNumbers() {
        var leftButton = findViewById<Button>(R.id.buttonLeft)
        var rightButton = findViewById<Button>(R.id.buttonRight)
        var scoreField = findViewById<TextView>(R.id.textViewScore)
        var scoreInfo = findViewById<TextView>(R.id.textViewInfo)

        if(counter>=0){
            var random =  Random()
            numLeft = random.nextInt(10)
            numRight = random.nextInt(10)

            //make sure numLeft != numRight
            do {
                numRight = random.nextInt(10)
            } while (numLeft == numRight)

            leftButton.text = "$numLeft"
            rightButton.text = "$numRight"
            scoreField.text = "Score: $counter"
        }

        else{
            scoreInfo.text = "Game over! Your take $score steps!"
            counter = 0
            leftButton.text = "0"
            rightButton.text = "0"
            //show button "Start new game"
            findViewById<Button>(R.id.buttonStartNewGame).visibility = View.VISIBLE
        }
    }
}
